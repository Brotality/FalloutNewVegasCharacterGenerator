package FalloutNewVegas;

public enum Gender {
	MALE(0), FEMALE(1);
	
	int ordinal;
	
	private Gender(int ord){
		this.ordinal = ord;
	}
	
	public static Gender getGender(int pos){
		for(Gender gend : Gender.values()){
			if(gend.ordinal == pos){
				return gend;
			}
		}
		return null;
	}
}
