package FalloutNewVegas;

public enum Race {
	AFRICAN_AMERICAN(0), ASIAN(1), CAUCASIAN(2), HISPANIC(3);
	
	int ordinal;
	
	private Race(int ord){
		this.ordinal = ord;
	}
	
	public static Race getRace(int pos){
		for(Race race : Race.values()){
			if(race.ordinal == pos){
				return race;
			}
		}
		return null;
	}
}
