package FalloutNewVegas;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class Character {

	private Integer age;
	private Gender gender;
	private Race race;
	private Map<Special, Integer> specials;
	private List<Skill> skills;
	private List<Trait> traits;
	
	public Character() {
		specials = new LinkedHashMap<Special, Integer>();
		skills = new ArrayList<Skill>();
		traits = new ArrayList<Trait>();
		Random rand = new Random();
		age = (rand.nextInt(10) + 1) * 10;
		gender = Gender.getGender(rand.nextInt(Gender.values().length));
		race = Race.getRace(rand.nextInt(Race.values().length));
		generateSpecials(rand);
		selectSkills(rand);
		selectTraits(rand);
	}
	
	private void generateSpecials(Random rand){
		for (Special spec : Special.values()){
			specials.put(spec, 1);
		}
		Special[] values = Special.values();
		int i = 33;
		while(i > 0){
			Special spec = Special.getSpecial(rand.nextInt(values.length));
			for(Map.Entry<Special, Integer> entry : specials.entrySet()){
				if(entry.getKey().equals(spec)){
					Integer val = entry.getValue();
					if(val == 10) break;
					val++;
					entry.setValue(val);
					i--;
				}
			}
		}
	}
	
	private void selectSkills(Random rand){
		Skill[] values = Skill.values();
		for(int a = 0; a < 2; a++){
			List<Skill> temp = new ArrayList<Skill>();
			int i = 3;
			while(i > 0){
				Skill skill = Skill.getSkill(rand.nextInt(values.length));
				if(!temp.contains(skill)){
					temp.add(skill);
					i--;
				}
			}
			for(Skill skill : temp){
				skills.add(skill);
			}
		}
	}
	
	private void selectTraits(Random rand){
		Trait[] values = Trait.values();
		int i = 2;
		while(i > 0){
			Trait trait = Trait.getTrait(rand.nextInt(values.length));
			if(!traits.contains(trait)){
				traits.add(trait);
				i--;
			}
		}
	}
	
	public static void main(String[] args){
		Character player = new Character();
		System.out.println("Gender: " + player.gender);
		System.out.println("Race: " + player.race);
		System.out.println("Age: " + player.age);
		System.out.println("Specials: " + player.specials);
		System.out.println("Skills: " + player.skills);
		System.out.println("Traits: " + player.traits);
	}

}
