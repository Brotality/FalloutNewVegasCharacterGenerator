package FalloutNewVegas;

public enum Skill {
	BARTER(0), ENERGY_WEAPONS(1), EXPLOSIVES(2), GUNS(3), LOCKPICK(4), MEDICINE(5),
	MELEE_WEAPONS(6), REPAIR(7), SCIENCE(8), SNEAK(9), SPEECH(10),
	SURVIVAL(11), UNARMED(12);
	
	int ordinal;
	
	private Skill(int ord){
		this.ordinal = ord;
	}
	
	public static Skill getSkill(int pos){
		for(Skill skill : Skill.values()){
			if(skill.ordinal == pos){
				return skill;
			}
		}
		return null;
	}
}
