package FalloutNewVegas;

public enum Trait {
	BUILT_TO_DESTROY(0), FAST_SHOT(1), FOUR_EYES(2), GOOD_NATURED(3), HEAVY_HANDED(4), KAMIKAZE(5),
	LOOSE_CANNON(6), SMALL_FRAME(7), TRIGGGER_DISCIPLINE(8), CLAUSTROPHOBIA(9), EARLY_BIRD(10),
	HOARDER(11), HOT_BLOODED(12), SKILLED(13);
	
	int ordinal;
	
	private Trait(int ord){
		this.ordinal = ord;
	}
	
	public static Trait getTrait(int pos){
		for(Trait trait : Trait.values()){
			if(trait.ordinal == pos){
				return trait;
			}
		}
		return null;
	}
}
