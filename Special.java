package FalloutNewVegas;

public enum Special {
	STENGTH(0), PERCEPTION(1), ENDURANCE(2), CHARISMA(3), INTELLIGENCE(4), AGILITY(5),
	LUCK(6);
	
	int ordinal;
	
	private Special(int ord){
		this.ordinal = ord;
	}
	
	public static Special getSpecial(int pos){
		for(Special spec : Special.values()){
			if(spec.ordinal == pos){
				return spec;
			}
		}
		return null;
	}
}
